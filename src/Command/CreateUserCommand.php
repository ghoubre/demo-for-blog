<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateUserCommand extends Command
{
    protected static $defaultName = 'app:create-user';

    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Command for create user')
            ->addArgument('firstName', InputArgument::REQUIRED, 'User firstname')
            ->addArgument('lastName', InputArgument::REQUIRED, 'User lastname')
            ->addArgument('age', InputArgument::OPTIONAL, 'User age Optional')

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Command Create User',
            '============'
        ]);

        $user = new User();

        $user->setFirstName($input->getArgument("firstName"));
        $output->writeln("With firstname : ". $user->getFirstName());
        $user->setLastName($input->getArgument("lastName"));
        $output->writeln("With lastname : ". $user->getLastName());

        $age = $input->getArgument("age");

        if (!$age) {
            $output->writeln("User doesn't have an age");
        } else {
            $user->setAge($age);
            $output->writeln("With Age : ". $user->getAge());
        }

        $this->manager->persist($user);
        $this->manager->flush();
        $output->writeln('Successful you have created the user : '. $user->getFirstName() . " ". $user->getLastName() . " " . $age);
    }
}
